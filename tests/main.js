/* globals gauge*/

const path = require('path');
const {
	openBrowser,
	closeBrowser,
	screenshot,
	goto,
	text,
	click,
	button,
} = require('taiko');

beforeSuite(async () => {
	await openBrowser({
		args: ['--no-sandbox', '--disable-setuid-sandbox'],
	});
});

afterSuite(async () => {
	await closeBrowser();
});

// Return a screenshot file name
gauge.customScreenshotWriter = async function () {
	const screenshotFilePath = path.join(
		process.env['gauge_screenshots_dir'],
		`screenshot-${process.hrtime.bigint()}.png`,
	);

	await screenshot({
		path: screenshotFilePath,
	});
	return path.basename(screenshotFilePath);
};

step('Open <link>', async (link) => {
	await goto(link);
});

step('When: I click <name> button', async (name) => {
	await click(button(name));
});

step('Then: I should see <name> header', async (name) => {
	await text(name).exists();
});
