const { goto, radioButton, text, button, click, waitFor } = require('taiko');

//Given: that "http://localhost:8080/quiz" isActive
step(
	'When: I land on <name> i want to see my main components on this page',
	async (name) => {
		await goto(name);
		await click(button('Start Quiz'));
	},
);

step('Then: I should see -Process information- as <name>', async (name) => {
	await waitFor(2000);
	await text(name).exists();
});

step(
	'Then: I should see <name> with unchecked answers component',
	async (name) => {
		await text(name).exists();
		await radioButton('A').exists();
		await radioButton('B').exists();
		await radioButton('C').exists();
		await radioButton('D').exists();
	},
);

step('Then: I should see <name> button inactive', async (name) => {
	const isDisabled = await button(name).isDisabled();

	if (!isDisabled) throw new Error(`${name} button must be disabled.`);
});

// Given: I solve atleast a question

step('When: I clicked a answer checkbox', async () => {
	await click(button('Start Quiz'));
	await click(radioButton('A'));
});

step('Then: I should see <name> become active', async (name) => {
	const isDisabled = await button(name).isDisabled();

	if (isDisabled) throw new Error(`${name} button must be active.`);
});

// Given: I finished my quiz

step('When: I click <name>', async (name) => {
	await click(button(name));
});

step('Then: I should see all <name> become invisible', async (name) => {
	const isInvisible = await text(name).exists();
	if (!isInvisible) throw new Error(`${name} text must be not there.`);
});

step(
	'Then: I should see -Process information- change to <name>',
	async (name) => {
		await waitFor(2000);
		await text(name).exists();
	},
);
