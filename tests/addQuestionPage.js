const {
	text,
	textBox,
	radioButton,
	button,
	click,
	waitFor,
	write,
	into,
	currentURL,
} = require('taiko');

// Given: that I am on "http://localhost:8080"
step('Then: I should see  -Process information- as <name>', async (name) => {
	await text(name).exists();
});

step('Then: I should see <name> textbox input that editable', async (name) => {
	await textBox(name).exists();
});

step(
	'Then: I should see <a>,<b>,<c>,<d> textbox input with checkbox',
	async (a, b, c, d) => {
		await textBox(a).exists();
		await radioButton(a).exists();
		await textBox(b).exists();
		await radioButton(b).exists();
		await textBox(c).exists();
		await radioButton(c).exists();
		await textBox(d).exists();
		await radioButton(d).exists();
	},
);

step(
	'Then: I should see <name> button inactive (cant click on it till all components filled)',
	async (name) => {
		const isDisabled = await button(name).isDisabled();

		if (!isDisabled) throw new Error(`${name} button must be disabled.`);
	},
);

// Given: I  fill my question components properly and 1 checkbox is  checked

step(
	'When: I fill my question components title: <name>, radio buttons: <a>,<b>,<c>,<d>',
	async (name, a, b, c, d) => {
		await click(button('Add Question'));
		await write('test', into(textBox(name)));
		await write('testa', into(textBox(a)));
		await write('testb', into(textBox(b)));
		await write('testc', into(textBox(c)));
		await write('testd', into(textBox(d)));
		await click(radioButton(a));
	},
);

step(
	'Then: I should see <name> button active(clickable) and click it',
	async (name) => {
		const isDisabled = await button(name).isDisabled();

		if (isDisabled) throw new Error(`${name} button must be active.`);

		await click(button(name));
	},
);

step(
	'Then: I should see -Process information- change to: <name>',
	async (name) => {
		await text(name).exists();
	},
);

step(
	'Then: I should see after 3 second  my  webpage index changed to: <link>',
	async (link) => {
		await waitFor(3500);
		const url = await currentURL();
		if (url !== link) throw new Error(`Current URL must be ${link}.`);
	},
);
