# Add question page acceptance test

* Open "http://34.118.115.9"

## Given: that I am on "http://34.118.115.9"

* When: I click "Add Question" button
* Then: I should see "Create Question" header
* Then: I should see  -Process information- as "Please fill empty areas for send your question"
* Then: I should see "Write your question" textbox input that editable
* Then: I should see "A","B","C","D" textbox input with checkbox
* Then: I should see "Send Question" button inactive (cant click on it till all components filled)

## Given: I  fill my question components properly and 1 checkbox is  checked

* When: I fill my question components title: "Write your question", radio buttons: "A","B","C","D"
* Then: I should see "Send Question" button active(clickable) and click it
* Then: I should see -Process information- change to: "Question sended"
* Then: I should see after 3 second  my  webpage index changed to: "http://34.118.115.9/"
