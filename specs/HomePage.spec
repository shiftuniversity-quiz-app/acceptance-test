# Home page acceptance test

* Open "http://34.118.115.9"

## Given: That I am on "http://34.118.115.9"
* When: The page is loaded totally "http://34.118.115.9/"
* Then: I should see "Quiz App" header
* Then: I should see "Add Question" button
* Then: I should see "Start Quiz" button

## Given : That i see "Add Question" button
* When: I click "Add Question" button
* Then: I should land on "http://34.118.115.9/add" page

## Given : That i see "Start Quiz" buttton
* When: I click "Start Quiz" button
* Then: I should land on "http://34.118.115.9/quiz" page
